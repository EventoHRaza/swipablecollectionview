//
//  SwipeView.swift
//  SwipeCollectionView
//
//  Created by HassanRazaKhalid on 6/8/17.
//  Copyright © 2017 IA. All rights reserved.
//

import UIKit

class SwipeView: UIView {

    @IBOutlet var tapButton: UIButton!
    
    
    static func getView() -> SwipeView {
        let view = Bundle.main.loadNibNamed("SwipeView", owner: nil, options: nil)?.first as! SwipeView
        return view
    }
    
//    @IBAction func viewTapped(_ sender: UIButton) {
    
//    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
