//
//  DummyCollectionCell.swift
//  SwipeCollectionView
//
//  Created by HassanRazaKhalid on 6/8/17.
//  Copyright © 2017 IA. All rights reserved.
//

import UIKit
import SwipeCollection

class DummyCollectionCell: UICollectionViewCell, HasLeftRightConstraint {
    
    @IBOutlet var leftConstraint: NSLayoutConstraint!
    @IBOutlet var rightConstraint: NSLayoutConstraint!
    @IBOutlet var containerView: UIView!
    var startPoint: CGPoint!
    var wasExpanded: Bool! = false

}

