//
//  ViewController.swift
//  SwipeCollectionView
//
//  Created by HassanRazaKhalid on 6/8/17.
//  Copyright © 2017 IA. All rights reserved.
//

import UIKit
import SwipeCollection

class ViewController: UIViewController {
    
    var swipeLogic: SwipeLogic!
    
    @IBOutlet var collectionView: UICollectionView!
    
    var dataSource = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        swipeLogic = SwipeLogic(sender: self)
        
        dataSource.append("1")
        dataSource.append("1")
        dataSource.append("1")
//        dataSource.append("1")
//        dataSource.append("1")
//        dataSource.append("1")
        // Do any additional setup after loading the view, typically from a nib.
        
        (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).itemSize = CGSize(width: 200, height: 300)
        addSwipeGesture()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func addSwipeGesture() {
        
        let swipeGesture = UIPanGestureRecognizer(target: self, action: #selector(panGestureReceived(_:)))
        swipeGesture.delegate = self
        collectionView.addGestureRecognizer(swipeGesture)
        
    }
    
    func panGestureReceived(_ recognizer: UIPanGestureRecognizer) {
        swipeLogic.handleSwipeLogic(recognizer: recognizer, collectionView: collectionView)
    }
}

extension ViewController: Swipeable {
    
    func swipeView(for cell: UICollectionViewCell, indexPath: IndexPath) -> [UIView]? {
        
        
        var swipeViews: [UIView]? = []
        
        let swipeView = SwipeView.getView()
        swipeView.tapButton.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
        swipeView.tapButton.tag = 1
        swipeViews?.append(swipeView)
        
        if indexPath.row == 0 {
            
            
        }
        else {
            let swipeView1 = SwipeView.getView()
            swipeView1.tapButton.addTarget(self, action: #selector(buttonPressed(sender:)), for: .touchUpInside)
            swipeView1.tapButton.tag = 2
            swipeViews?.append(swipeView1)
        }

        return swipeViews
    }
    
    func buttonPressed(sender: UIButton) {
        
        guard let cell: UICollectionViewCell = sender.findParentCell() else {
            return
        }
        guard let indexPath = collectionView.indexPath(for: cell) else {
            return
        }
        print("\(indexPath.row)" + "Btn Tag: \(sender.tag)")
    }
}
extension ViewController:UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}
extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        swipeLogic.checkForSwipingContainer(for: cell, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! DummyCollectionCell
        let color = UIColor.red
        cell.containerView.backgroundColor = color
        return cell
    }
}

extension UIView {
    
    func findParentCell<T: UIView>() -> T? {
        
        var aView = self.superview
        while aView != nil {
            
            if let tmpView = aView as? T {
                return tmpView
            }
            aView = aView?.superview
        }
        return nil; // this v
    }
}

