//
//  Swipeable.swift
//  SwipeCollectionView
//
//  Created by HassanRazaKhalid on 6/8/17.
//  Copyright © 2017 IA. All rights reserved.
//

import Foundation
import UIKit

let SwipeContainerViewTag = 999

public protocol Swipeable {
 
    func swipeView(for cell: UICollectionViewCell, indexPath: IndexPath) -> [UIView]?
}


public protocol HasLeftRightConstraint: class {
    
    var leftConstraint: NSLayoutConstraint! { get }
    var rightConstraint: NSLayoutConstraint! { get }
    
    var containerView: UIView! { get }
    
    var startPoint: CGPoint! { get set }
    var wasExpanded: Bool! { get set }
}
public extension HasLeftRightConstraint {
    
    var isExpanded: Bool {
        return !(rightConstraint.constant == 0)
    }
    
    func closeSlidingView() {
        rightConstraint.constant = 0
        leftConstraint.constant = 0
    }
}

extension CGFloat {
    
    func isMoreThanHalf(value: CGFloat) -> Bool {
        return self/value >= 0.5
    }
}
