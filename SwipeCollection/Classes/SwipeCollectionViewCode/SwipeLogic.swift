//
//  SwipeLogic.swift
//  Pods
//
//  Created by HassanRazaKhalid on 6/11/17.
//
//

import Foundation
import UIKit

public class SwipeLogic {
    
    var oldCell: HasLeftRightConstraint?
    
    var swipeDelegate: Swipeable?
    var isArabic: Bool
    
    public init(sender: Swipeable, isArabic: Bool = true) {
        self.swipeDelegate = sender
        self.isArabic = isArabic
    }
    
    public func handleSwipeLogic(recognizer: UIPanGestureRecognizer, collectionView: UICollectionView) {
        
        let point = recognizer.location(in: collectionView)
        guard let indexpath = collectionView.indexPathForItem(at: point) else {
            return
        }
        guard var swipable = collectionView.cellForItem(at: indexpath) as? HasLeftRightConstraint else{
            return
        }
        
        if  swipable !== oldCell {
            oldCell?.closeSlidingView()
        }
        
        let cell = swipable as! UICollectionViewCell
        let stackView = cell.viewWithTag(SwipeContainerViewTag)!
        
        let actionConstraint = swipable.rightConstraint!
        let actionConstraintValue = abs(actionConstraint.constant)
        
        switch recognizer.state {
        case .began:
            
            swipable.wasExpanded = swipable.isExpanded
            
            swipable.startPoint = collectionView.convert(point, to: cell)
            //            print("In begin")
            
        case .changed:
            
            //            print("In Changed")
            let currentPoint =  collectionView.convert(point, to: cell)
            if swipable.startPoint == nil {
                swipable.startPoint = currentPoint
            }
            let deltaX = currentPoint.x - swipable.startPoint.x
            var panningleft = false
            
            if currentPoint.x < swipable.startPoint.x{
                panningleft = true
            }
            let absDeltaX = abs(deltaX)
            
            
            let rightDirLogic = {
                let finalValue = actionConstraintValue < stackView.frame.width ? absDeltaX : 0
                
                swipable.leftConstraint!.constant -= finalValue
                swipable.rightConstraint!.constant += finalValue
                
            }
            
            let leftDirLogic = {
                let finalValue = swipable.rightConstraint!.constant  > 0 ? absDeltaX : 0
                
                swipable.leftConstraint!.constant += finalValue
                swipable.rightConstraint!.constant -= finalValue
            }
            
            if isArabic {
                
                if panningleft {
                    leftDirLogic()
                    
                }
                else {
                    rightDirLogic()
                }
            }
            else {
                if panningleft {
                    rightDirLogic()
                }
                else {
                    leftDirLogic()
                }
            }
            
            swipable.startPoint = currentPoint
        case .cancelled,.failed:
            
            if swipable.wasExpanded {
                
                let finalValue = stackView.frame.width
                
                swipable.leftConstraint!.constant = -finalValue
                swipable.rightConstraint!.constant = finalValue
            }
            else {
                swipable.closeSlidingView()
            }
        case .ended:
            
            //            print("In Ended")
            if actionConstraintValue.isMoreThanHalf(value: stackView.frame.width) {
                
                let finalValue = stackView.frame.width
                swipable.leftConstraint!.constant = -finalValue
                swipable.rightConstraint!.constant = finalValue
                
                //                actionConstraint.constant = stackView.frame.width
            }
            else {
                
                swipable.closeSlidingView()
                //                actionConstraint.constant = 0
            }
        default:
            break
        }
        oldCell = swipable
    }
    
    public func checkForSwipingContainer(for cell: UICollectionViewCell, indexPath: IndexPath) {
        
        guard let leftRight = cell as? HasLeftRightConstraint else {
            return
        }
        
        let viewsList = swipeDelegate?.swipeView(for: cell, indexPath: indexPath)
        
        var stackView: UIStackView!
        if let tmpStackView = cell.viewWithTag(SwipeContainerViewTag) as? UIStackView {
            stackView = tmpStackView
        }
        else {
            // Insert stackView
            stackView = UIStackView()
            stackView.axis = .horizontal
            stackView.distribution = .fillEqually
            stackView.translatesAutoresizingMaskIntoConstraints = false
            stackView.tag = SwipeContainerViewTag
            stackView.backgroundColor = UIColor.blue
            //            stackView.addToParent(parentView: cell.contentView, index: 0, edges: .zero)
            cell.contentView.insertSubview(stackView, at: 0)
            
            stackView.leadingAnchor.constraint(equalTo: leftRight.containerView.trailingAnchor).isActive = true
            stackView.topAnchor.constraint(equalTo: cell.contentView.topAnchor).isActive = true
            stackView.bottomAnchor.constraint(equalTo: cell.contentView.bottomAnchor).isActive = true
            
            
            stackView.layoutIfNeeded()
            //            let backMost cell.subviews.first!
        }
        stackView?.replaceViews(views: viewsList)
    }
}
