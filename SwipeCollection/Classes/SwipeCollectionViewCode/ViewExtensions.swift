//
//  ViewExtensions.swift
//  SwipeCollectionView
//
//  Created by HassanRazaKhalid on 6/8/17.
//  Copyright © 2017 IA. All rights reserved.
//

import Foundation
import UIKit

extension UIStackView {
    
    func replaceViews(views: [UIView]?) {
        if let tmpViews = views {
            clearAllView()
            addArrangedSubviews(views: tmpViews)
        }
        else {
            clearAllView()
        }
    }
    
    func clearAllView() {
        
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
    }
    
    func addArrangedSubviews(views: [UIView]) {
        
        for view in views {
            self.addArrangedSubview(view)
        }
    }

}
