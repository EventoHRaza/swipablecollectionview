# SwipeCollection

[![CI Status](http://img.shields.io/travis/HassanRazaKhalid/SwipeCollection.svg?style=flat)](https://travis-ci.org/HassanRazaKhalid/SwipeCollection)
[![Version](https://img.shields.io/cocoapods/v/SwipeCollection.svg?style=flat)](http://cocoapods.org/pods/SwipeCollection)
[![License](https://img.shields.io/cocoapods/l/SwipeCollection.svg?style=flat)](http://cocoapods.org/pods/SwipeCollection)
[![Platform](https://img.shields.io/cocoapods/p/SwipeCollection.svg?style=flat)](http://cocoapods.org/pods/SwipeCollection)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

SwipeCollection is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "SwipeCollection"
```

## Author

HassanRazaKhalid, evento.h.raza@ia.gov.ae

## License

SwipeCollection is available under the MIT license. See the LICENSE file for more info.
